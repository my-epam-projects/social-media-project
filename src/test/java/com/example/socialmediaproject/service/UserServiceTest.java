package com.example.socialmediaproject.service;

import com.example.socialmediaproject.dto.user.UserCreateDTO;
import com.example.socialmediaproject.dto.user.UserDTO;
import com.example.socialmediaproject.mapper.UserMapper;
import com.example.socialmediaproject.model.User;
import com.example.socialmediaproject.repository.UserRepository;
import com.example.socialmediaproject.service.impl.UserServiceImpl;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.when;

@SpringBootTest
class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private UserMapper userMapper;

    @InjectMocks
    private UserServiceImpl userService;

    @Test
    void testGetAllUsers() {
        // Arrange
        List<User> mockUsers = Arrays.asList(new User(), new User()); // Add mock users as needed
        when(userRepository.findAll()).thenReturn(mockUsers);

        List<UserDTO> mockUserDTOs = Arrays.asList(new UserDTO(), new UserDTO()); // Add corresponding mock DTOs
        when(userMapper.toDtoList(anyList())).thenReturn(mockUserDTOs);

        // Act
        List<UserDTO> result = userService.getAllUsers();

        // Assert
        assertNotNull(result);
        assertEquals(mockUserDTOs.size(), result.size());
    }

    @Test
    void testGetUserById() {
        // Arrange
        Long userId = 1L;
        User mockUser = new User(); // Create a mock user
        when(userRepository.findById(userId)).thenReturn(Optional.of(mockUser));

        UserDTO mockUserDTO = new UserDTO(); // Create a corresponding mock DTO
        when(userMapper.toDto(any(User.class))).thenReturn(mockUserDTO);

        // Act
        UserDTO result = userService.getUserById(userId);

        // Assert
        assertNotNull(result);
    }

    @Test
    void testCreateUser() {
        // Arrange
        UserCreateDTO mockUserCreateDTO = new UserCreateDTO(); // Create a mock DTO
        User mockUserToSave = new User(); // Create a mock user
        when(userMapper.toEntity(mockUserCreateDTO)).thenReturn(mockUserToSave);

        when(userRepository.save(any(User.class))).thenReturn(mockUserToSave);

        UserDTO mockUserDTO = new UserDTO(); // Create a corresponding mock DTO
        when(userMapper.toDto(mockUserToSave)).thenReturn(mockUserDTO);

        // Act
        UserDTO result = userService.createUser(mockUserCreateDTO);

        // Assert
        assertNotNull(result);
    }


    @Test
    void testFollowUser() {
        // Arrange
        Long followerId = 1L;
        Long followedId = 2L;
        User mockFollower = new User();
        mockFollower.setId(followedId);// Create a mock follower
        User mockFollowed = new User();
        mockFollowed.setId(followedId);// Create a mock followed
        when(userRepository.findById(followerId)).thenReturn(Optional.of(mockFollower));
        when(userRepository.findById(followedId)).thenReturn(Optional.of(mockFollowed));

        // Act
        assertDoesNotThrow(() -> userService.followUser(followerId, followedId));

        // You can add more assertions based on your specific requirements
    }


}
