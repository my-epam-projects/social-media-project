package com.example.socialmediaproject.service;

import com.example.socialmediaproject.dto.like.LikeCreateDTO;
import com.example.socialmediaproject.dto.post.PostCreateDTO;
import com.example.socialmediaproject.dto.post.PostDTO;
import com.example.socialmediaproject.mapper.PostMapper;
import com.example.socialmediaproject.model.Like;
import com.example.socialmediaproject.model.Post;
import com.example.socialmediaproject.model.User;
import com.example.socialmediaproject.repository.LikeRepository;
import com.example.socialmediaproject.repository.PostRepository;
import com.example.socialmediaproject.service.impl.PostServiceImpl;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
class PostServiceTest {


    @Mock
    private PostRepository postRepository;

    @Mock
    private LikeRepository likeRepository;

    @Mock
    private PostMapper postMapper;

    @Mock
    private UserService userService;

    @InjectMocks
    private PostServiceImpl postService;

    @Test
    void testGetAllPosts() {
        // Arrange
        List<Post> mockPosts = new ArrayList<>(); // Add mock posts as needed
        when(postRepository.findAll()).thenReturn(mockPosts);

        List<PostDTO> mockPostDTOs = new ArrayList<>(); // Add corresponding mock DTOs
        when(postMapper.toDtoList(Collections.singletonList(any(Post.class)))).thenReturn(mockPostDTOs);

        // Act
        List<PostDTO> result = postService.getAllPosts();

        // Assert
        assertNotNull(result);
        assertEquals(mockPostDTOs.size(), result.size());
    }

    @Test
    void testGetPostById() {
        // Arrange
        Long postId = 1L;
        Post mockPost = new Post();
        mockPost.setId(postId);
        when(postRepository.findById(postId)).thenReturn(Optional.of(mockPost));

        PostDTO mockPostDTO = new PostDTO(); // Create a corresponding mock DTO
        when(postMapper.toDto(any(Post.class))).thenReturn(mockPostDTO);

        // Act
        PostDTO result = postService.getPostById(postId);

        // Assert
        assertNotNull(result);
    }

    @Test
    void testCreatePost() {
        // Arrange
        PostCreateDTO mockPostCreateDTO = new PostCreateDTO("title", "body", 1L); // Create a mock DTO
        Post mockPost = new Post(1L, "sdads", "asdasd", new User(), Set.of(new Like(), new Like())); // Create a mock post
        when(postMapper.toEntity(mockPostCreateDTO)).thenReturn(mockPost);

        User mockAuthor = new User();
        mockAuthor.setId(1L);
        mockAuthor.setUsername("username");// Create a mock author
        when(userService.findById(any(Long.class))).thenReturn(mockAuthor);

        when(postRepository.save(any(Post.class))).thenReturn(mockPost);

        PostDTO mockPostDTO = new PostDTO(); // Create a corresponding mock DTO
        when(postMapper.toDto(mockPost)).thenReturn(mockPostDTO);

        // Act
        PostDTO result = postService.createPost(mockPostCreateDTO);

        // Assert
        assertNotNull(result);
    }

    @Test
    void testLikePost() {
        // Arrange
        LikeCreateDTO mockLikeCreateDTO = new LikeCreateDTO(1L,2L); // Create a mock like DTO
        when(postRepository.findById(any(Long.class))).thenReturn(Optional.of(new Post())); // Assume post exists
        when(userService.findById(any(Long.class))).thenReturn(new User()); // Assume user exists
        when(likeRepository.save(any(Like.class))).thenReturn(new Like()); // Assume like is saved

        // Act & Assert
        assertDoesNotThrow(() -> postService.likePost(mockLikeCreateDTO));

        // You can add more assertions based on your specific requirements
    }


}
