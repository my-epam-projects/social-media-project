package com.example.socialmediaproject.controller;

import com.example.socialmediaproject.dto.like.LikeCreateDTO;
import com.example.socialmediaproject.dto.like.LikeDTO;
import com.example.socialmediaproject.dto.post.PostCreateDTO;
import com.example.socialmediaproject.dto.post.PostDTO;
import com.example.socialmediaproject.dto.user.UserDTO;
import com.example.socialmediaproject.service.PostService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;
import java.util.Set;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(PostController.class)
class PostControllerTest {


    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PostService postService;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void testGetAllPosts() throws Exception {
        // Arrange
        List<PostDTO> posts = List.of(new PostDTO(1L, "sdads", "asdasd", new UserDTO(), Set.of(new LikeDTO(), new LikeDTO())));
        when(postService.getAllPosts()).thenReturn(posts);

        // Act
        ResultActions result = mockMvc.perform(get("/posts"));

        // Assert
        result.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray());
    }

    @Test
    void testGetPostById() throws Exception {
        // Arrange
        Long postId = 1L;
        PostDTO postDTO = new PostDTO(postId, "sdads", "asdasd", new UserDTO(), Set.of(new LikeDTO(), new LikeDTO()));
        when(postService.getPostById(postId)).thenReturn(postDTO);

        // Act
        ResultActions result = mockMvc.perform(get("/posts/{id}", postId));

        // Assert
        result.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(postId));
    }

    @Test
    void testCreatePost() throws Exception {
        // Arrange
        PostCreateDTO postCreateDTO = new PostCreateDTO("title","body",1L);
        PostDTO createdPostDTO = new PostDTO(1L, "title", "body", new UserDTO(), Set.of(new LikeDTO(), new LikeDTO()));
        when(postService.createPost(any(PostCreateDTO.class))).thenReturn(createdPostDTO);

        // Act
        ResultActions result = mockMvc.perform(post("/posts")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(postCreateDTO)));

        // Assert
        result.andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(createdPostDTO.getId()));
    }

    @Test
    void testLikePost() throws Exception {
        // Arrange
        LikeCreateDTO likeCreateDTO = new LikeCreateDTO(/* Add necessary fields */);
        doNothing().when(postService).likePost(any(LikeCreateDTO.class));

        // Act
        ResultActions result = mockMvc.perform(post("/posts/like")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(likeCreateDTO)));

        // Assert
        result.andExpect(status().isOk());
    }


}
