package com.example.socialmediaproject.controller;

import com.example.socialmediaproject.dto.user.UserCreateDTO;
import com.example.socialmediaproject.dto.user.UserDTO;
import com.example.socialmediaproject.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(UserController.class)
class UserControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
     void testGetAllUsers() throws Exception {
        // Arrange
        List<UserDTO> users = List.of(new UserDTO(1L,"username"),new UserDTO(2L,"username"));
        when(userService.getAllUsers()).thenReturn(users);

        // Act
        ResultActions result = mockMvc.perform(get("/users"));

        // Assert
        result.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray());
    }

    @Test
     void testGetUserById() throws Exception {
        // Arrange
        Long userId = 1L;
        UserDTO userDTO = new UserDTO(userId,"username");
        when(userService.getUserById(userId)).thenReturn(userDTO);

        // Act
        ResultActions result = mockMvc.perform(get("/users/{id}", userId));

        // Assert
        result.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(userId));
    }

    @Test
     void testCreateUser() throws Exception {
        // Arrange
        UserCreateDTO userCreateDTO = new UserCreateDTO("username");
        UserDTO createdUserDTO = new UserDTO(1L,userCreateDTO.getUsername());
        when(userService.createUser(any(UserCreateDTO.class))).thenReturn(createdUserDTO);

        // Act
        ResultActions result = mockMvc.perform(post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(userCreateDTO)));

        // Assert
        result.andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(createdUserDTO.getId()));
    }

    @Test
     void testFollowUser() throws Exception {
        // Arrange
        Long followerId = 1L;
        Long followedId = 2L;

        // Act
        ResultActions result = mockMvc.perform(post("/users/follow/{followerId}/{followedId}", followerId, followedId));

        // Assert
        result.andExpect(status().isOk());
    }


}
