package com.example.socialmediaproject.service.impl;

import com.example.socialmediaproject.dto.user.UserCreateDTO;
import com.example.socialmediaproject.dto.user.UserDTO;
import com.example.socialmediaproject.mapper.UserMapper;
import com.example.socialmediaproject.model.User;
import com.example.socialmediaproject.repository.UserRepository;
import com.example.socialmediaproject.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final UserMapper mapper;

    public List<UserDTO> getAllUsers() {
        return mapper.toDtoList(userRepository.findAll());
    }

    public UserDTO getUserById(Long id) {
        Optional<User> user = userRepository.findById(id);
        return user.map(mapper::toDto).orElse(null);
    }

    public UserDTO createUser(UserCreateDTO user) {
        User userToSave = mapper.toEntity(user);
        userRepository.save(userToSave);
        return mapper.toDto(userToSave);
    }

    public void followUser(Long followerId, Long followedId) {
        User follower = userRepository.findById(followerId).orElse(null);
        User followed = userRepository.findById(followedId).orElse(null);

        if (follower != null && followed != null) {
            follower.getFollowers().add(followed);
            userRepository.save(follower);
        }
    }

    public User findById(Long id) {
        Optional<User> user = userRepository.findById(id);
        return user.get();
    }
}
