package com.example.socialmediaproject.service.impl;

import com.example.socialmediaproject.dto.like.LikeCreateDTO;
import com.example.socialmediaproject.dto.post.PostCreateDTO;
import com.example.socialmediaproject.dto.post.PostDTO;
import com.example.socialmediaproject.mapper.PostMapper;
import com.example.socialmediaproject.model.Like;
import com.example.socialmediaproject.model.Post;
import com.example.socialmediaproject.model.User;
import com.example.socialmediaproject.repository.LikeRepository;
import com.example.socialmediaproject.repository.PostRepository;
import com.example.socialmediaproject.service.PostService;
import com.example.socialmediaproject.service.UserService;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PostServiceImpl implements PostService {


    private final PostRepository postRepository;
    private final LikeRepository likeRepository;

    private final PostMapper postMapper;
    private final UserService userService;

    public List<PostDTO> getAllPosts() {
        List<Post> posts = postRepository.findAll();
        return posts.stream()
                .map(postMapper::toDto)
                .collect(Collectors.toList());
    }

    public PostDTO getPostById(Long id) {
        Optional<Post> postOptional = postRepository.findById(id);

        if (postOptional.isPresent()) {
            Post post = postOptional.get();
            return postMapper.toDto(post);
        } else {
            throw new EntityNotFoundException("Post not found with ID: " + id);
        }
    }

    public PostDTO createPost(PostCreateDTO dto) {
        Post post = postMapper.toEntity(dto);

        User author = userService.findById(dto.getAuthorId());
        if (author == null) {
            throw new EntityNotFoundException("User not found with ID: " + dto.getAuthorId());
        }
        post.setAuthor(author);
        Post savedPost = postRepository.save(post);
        return postMapper.toDto(savedPost);
    }

    public void likePost(LikeCreateDTO dto) {
        Optional<Post> postOptional = postRepository.findById(dto.getPostId());
        Optional<User> userOptional = Optional.ofNullable(userService.findById(dto.getUserId()));

        if (postOptional.isPresent() && userOptional.isPresent()) {
            Post post = postOptional.get();
            User user = userOptional.get();

            // Check if the user has already liked the post
            if (post.getLikes().stream().noneMatch(like -> like.getLikedUser().equals(user))) {
                Like like = new Like();
                like.setLikedPost(post);
                like.setLikedUser(user);
                post.getLikes().add(like);
                likeRepository.save(like);
            } else {
                throw new IllegalStateException("User has already liked the post");
            }
        } else {
            throw new EntityNotFoundException("Post or User not found with provided IDs");
        }
    }
}
