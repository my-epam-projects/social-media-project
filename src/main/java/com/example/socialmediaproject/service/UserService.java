package com.example.socialmediaproject.service;

import com.example.socialmediaproject.dto.user.UserCreateDTO;
import com.example.socialmediaproject.dto.user.UserDTO;
import com.example.socialmediaproject.model.User;

import java.util.List;

public interface UserService {
    List<UserDTO> getAllUsers();

    UserDTO getUserById(Long id);

    UserDTO createUser(UserCreateDTO dto);

    void followUser(Long followerId, Long followedId);

    User findById(Long id);
}
