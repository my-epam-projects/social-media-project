package com.example.socialmediaproject.service;

import com.example.socialmediaproject.dto.like.LikeCreateDTO;
import com.example.socialmediaproject.dto.post.PostCreateDTO;
import com.example.socialmediaproject.dto.post.PostDTO;

import java.util.List;

public interface PostService {

    List<PostDTO> getAllPosts();

    PostDTO getPostById(Long id);

    PostDTO createPost(PostCreateDTO dto);

    void likePost(LikeCreateDTO dto);
}
