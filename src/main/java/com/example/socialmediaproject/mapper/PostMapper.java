package com.example.socialmediaproject.mapper;

import com.example.socialmediaproject.dto.post.PostCreateDTO;
import com.example.socialmediaproject.dto.post.PostDTO;
import com.example.socialmediaproject.model.Post;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class PostMapper {

    private final UserMapper userMapper;
    private final LikeMapper likeMapper;

    public PostDTO toDto(Post post) {
        return new PostDTO(post.getId(),
                post.getTitle(),
                post.getBody(),
                userMapper.toDto(post.getAuthor()), likeMapper.toDotList(post.getLikes()));
    }

    public List<PostDTO> toDtoList(List<Post> postList) {
        return postList.stream().map(this::toDto).collect(Collectors.toList());
    }

    public Post toEntity(PostCreateDTO dto) {
        Post post = new Post();
        post.setTitle(dto.getTitle());
        post.setBody(dto.getBody());
        return post;
    }
}
