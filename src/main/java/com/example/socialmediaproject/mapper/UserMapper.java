package com.example.socialmediaproject.mapper;

import com.example.socialmediaproject.dto.user.UserCreateDTO;
import com.example.socialmediaproject.dto.user.UserDTO;
import com.example.socialmediaproject.model.User;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserMapper {


    public UserDTO toDto(User user) {
        return new UserDTO(user.getId(), user.getUsername());
    }

    public List<UserDTO> toDtoList(List<User> userList) {
        return userList.stream().map(user -> new UserDTO(user.getId(), user.getUsername())).collect(Collectors.toList());
    }

    public User toEntity(UserCreateDTO dto) {
        User user = new User();
        user.setUsername(dto.getUsername());
        return user;
    }
}
