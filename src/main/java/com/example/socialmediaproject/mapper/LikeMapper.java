package com.example.socialmediaproject.mapper;

import com.example.socialmediaproject.dto.like.LikeCreateDTO;
import com.example.socialmediaproject.dto.like.LikeDTO;
import com.example.socialmediaproject.model.Like;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class LikeMapper {

    private final UserMapper userMapper;

    public LikeDTO toDto(Like like) {
        return new LikeDTO(userMapper.toDto(like.getLikedUser()));
    }

    public Set<LikeDTO> toDotList(Set<Like> likes) {
        return likes.stream().map(this::toDto).collect(Collectors.toSet());
    }
}
