package com.example.socialmediaproject.dto.post;

import com.example.socialmediaproject.dto.like.LikeDTO;
import com.example.socialmediaproject.dto.user.UserDTO;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PostDTO {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("title")
    private String title;

    @JsonProperty("body")
    private String body;

    @JsonProperty("user")
    private UserDTO author;

    @JsonProperty("likes")
    private Set<LikeDTO> likes;
}
