package com.example.socialmediaproject.dto.like;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.JoinColumn;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class LikeCreateDTO {

    @JsonProperty("user_id")
    private Long userId;

    @JsonProperty("post_id")
    private Long postId;
}
