package com.example.socialmediaproject.dto.like;

import com.example.socialmediaproject.dto.user.UserDTO;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
public class LikeDTO {

    @JsonProperty("liked_user")
    private UserDTO likedUser;
}
