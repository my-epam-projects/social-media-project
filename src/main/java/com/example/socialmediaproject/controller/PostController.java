package com.example.socialmediaproject.controller;

import com.example.socialmediaproject.dto.like.LikeCreateDTO;
import com.example.socialmediaproject.dto.post.PostCreateDTO;
import com.example.socialmediaproject.dto.post.PostDTO;
import com.example.socialmediaproject.model.Post;
import com.example.socialmediaproject.service.PostService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/posts")
public class PostController {

    private final PostService postService;

    @GetMapping
    public List<PostDTO> getAllPosts() {
        return postService.getAllPosts();
    }

    @GetMapping("/{id}")
    public PostDTO getPostById(@PathVariable Long id) {
        return postService.getPostById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public PostDTO createPost(@RequestBody PostCreateDTO dto) {
        return postService.createPost(dto);
    }

    @PostMapping("/like")
    public void likePost(@RequestBody LikeCreateDTO dto) {
        postService.likePost(dto);
    }
}
